$(document).ready(function() {
(function() {
  var cup1 = $("#cup1"),
    cup2 = $("#cup2"),
    cup3 = $("#cup3"),
    ball = $("#ball"),
    btnPlay = $("#play"),
    btnReset = $("#reset"),
    btnlvl1 = $("#lvl1"),
    btnlvl2 = $("#lvl2"),
    btnlvl3 = $("#lvl3"),
    z = 0;
    a = document.getElementById("notif");

btnReset.on("click", function(event){
    btnPlay.attr('disabled', false);
    btnReset.attr('disabled', true);
    a.style.backgroundColor = "white";
    a.innerHTML = "";
    ball.hide();
    btnlvl1.attr('disabled', false);
    btnlvl2.attr('disabled', false);
    btnlvl3.attr('disabled', false);
    btnlvl1.prop("checked", false);
    btnlvl2.prop("checked", false);
    btnlvl3.prop("checked", false); 
});
  btnPlay.on("click", function(event) {
    if (btnlvl1.is(':checked')) {
            var shuffleSpeed = 1000;
            var numberOfShuffles = 10;
            btnPlay.attr('disabled', false);
        } else if (btnlvl2.is(':checked')) {
            var shuffleSpeed = 600;
            var numberOfShuffles = 15;
            btnPlay.attr('disabled', false);
        } else if (btnlvl3.is(':checked')) {
            var shuffleSpeed = 200;
            var numberOfShuffles = 30;
            btnPlay.attr('disabled', false);
        } else if (btnlvl1.prop("checked", false) && btnlvl2.prop("checked", false) && btnlvl3.prop("checked", false)){
            alert("You need to select a level first!");
            btnReset();
        }
    btnlvl1.attr('disabled', true);
    btnlvl2.attr('disabled', true);
    btnlvl3.attr('disabled', true);
    event.preventDefault();
    btnPlay.attr('disabled', true);
    var ans = Math.floor(Math.random() * 3) + 1; //chooses a random cup
    var ballInitialPosition = 0;
    //Show the ball
    ball.show();
    // Update the initial position based on the random number
    ballInitialPosition = 15 + ((ans - 1) * 200);

    // Move ball under the relative cup based on random number
    ball.css({
      left: ballInitialPosition + "px"
    });

//Drop the ball
    ball.animate({
      top: "220px"
    }, {
      duration: 1500,
      specialEasing: {
        top: 'easeOutBounce'
      },
      complete: function() {
        ball.html("<img src='img/ball.png'/>");
        ball.animate({
      top: "220px"
    }, {
      duration: 1000,
      specialEasing: {
        top: 'easeOutBounce'
      },
          complete: function() {
//Cover the ball using the cups
            cup1.delay(100).queue(function(n) {
              $(this).html("<img src='img/cup.png'/>");
              if(ans == 1) ball.hide();
              n();
            });
            cup2.delay(200).queue(function(n) {
              $(this).html("<img src='img/cup.png'/>");
              if(ans == 2) ball.hide();
              n();
            });
            cup3.delay(300).queue(function(n) {
              $(this).html("<img src='img/cup.png'/>");
              if(ans == 3) ball.hide();


              var cup1_left = cup1.position().left,
                cup2_left = cup2.position().left,
                cup3_left = cup3.position().left,
                cup_top = cup3.position().top;

              cup1.css({
                position: "absolute",
                top: cup_top + "px",
                left: cup1_left + "px"
              });

              cup2.css({
                position: "absolute",
                top: cup_top + "px",
                left: cup2_left + "px"
              });

              cup3.css({
                position: "absolute",
                top: cup_top + "px",
                left: cup3_left + "px"
              });

              shuffle = function(o) {
                for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                return o;
              };

              var interval = setInterval(function() {
                var array = shuffle([1, 2, 3]);

                $("#cup" + array[0]).animate({
                  left: $("#cup" + array[1]).position().left + "px"
                }, {
                  duration: shuffleSpeed / 2,
                  specialEasing: {
                    left: 'easeInQuint'
                  }
                });

                $("#cup" + array[1]).animate({
                  left: $("#cup" + array[0]).position().left + "px"
                }, {
                  duration: shuffleSpeed / 2,
                  specialEasing: {
                    left: 'easeInQuint'
                  }
                });


              }, shuffleSpeed);


              setTimeout(function() {
                clearInterval(interval);
                var flag = 0;
                $('div[id^="cup"]').css("cursor", "pointer");

                cup1.click(function() {
                  if(flag == 0) {
                    $(this).html('<img src="img/cup.png" id="cup">');
                    $(this).append(' <img src="img/cup.png" id="cup">');
                    if(ans == 1) {
                      ball.css({
                        left: $(this).position().left + 46 + "px"
                      });
                      flag = 1;
                      checkWin();
                    } else {
                      checkLose();
                      flag = 1;
                    }
                  }
                });

                cup2.click(function() {
                  if(flag == 0) {
                    $(this).html('<img src="img/cup.png" id="cup">');
                    $(this).append(' <img src="img/cup.png" id="cup">');
                    if(ans == 2) {
                      ball.css({
                        left: $(this).position().left + 46 + "px"
                      });
                      flag = 1;
                      checkWin();
                    } else {
                      flag = 1;
                      checkLose();
                    }
                  }
                });

                $("#cup3").click(function() {
                  if(flag == 0) {
                    $(this).html('<img src="img/cup.png" id="cup">');
                    $(this).append(' <img src="img/cup.png" id="cup">');
                    if(ans == 3) {
                      ball.css({
                        left: $(this).position().left + 46 + "px"
                      });
                      flag = 1;
                      checkWin();
                    } else {
                      flag = 1;
                      checkLose();
                    }
                  }
                });


                function checkWin() {
                    ball.show();
                    a.style.backgroundColor = "green";
                    a.innerHTML = "You Win!";
                }
                function checkLose() { 
                    ball.show();
                    a.style.backgroundColor = "red";
                    a.innerHTML = "You Lose!";
                }
              }, numberOfShuffles * shuffleSpeed);
              n();
            });
          }
        });
      }
    });
  });


})();


});

