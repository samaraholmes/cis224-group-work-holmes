Samara Holmes Final Web Development Project

    This concentration game is a version of the magic cup game. There were multiple animations necessary to create this game so I found a reference file online to go by. The reference file contains a complete game using boxes and a cartoon character. 
    I wanted to actually enjoy making this game and also not just copy someone's code, so to make this my own, I have used a cup and ball rather than boxes and a cartoon. Not only that, here were also multiple other functions with concepts of javascript and jQuery that I never learned, so I did not include functions not necessary to the game play. There are lots of style changes in the css because obviously the cup images are not the same size as the reference file images. There was a lot of trial and error in aligning the ball and the cup when it is dropped before the start of the game. 
    There is a checkWin() and checkLose() function that when true, change the color of the footer to either green or red respectively. 
    There is also a play and a reset button which when clicked, become disabled and enable the other. The reset button has the same function as if you were clicking the refresh button on the browser. The play button only works if fully reset.
    Note: There is a bug with the reset button. I couldn't manage to get it to fully work so you are better off just hitting the refresh button. Currently it just resets the page but it does not reset the ball and where it goes.
    I also added in a 3 level selection which changes the speed of the shuffling. 
    